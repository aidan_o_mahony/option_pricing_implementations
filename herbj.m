%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                     %
%   Implementation of pricing of options on the       %
%   minimum or maximum of serveral assets based on    %
%   the paper by Herb Johnson titled                  %
%   "Options on the Maximum or the Minimum of         %
%   Several Assets" from 1987                         %
%                                                     %
%   Implemented by Aidan O Mahony, University         %
%   College Cork, Ireland.                            %
%   Email - 103837793@umail.ucc.ie                    %
%                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%% Values taken from paper %%%%%
n      =   2; % number of assets
S1     =  40; % asset price of S1
S2     =  40; % asset price of S2
X      =  40; % Exercise price
r      = 0.1; % Risk free rate 
sigma1 = 0.3; % Standard deviation
sigma2 = 0.3; % Standard deviation
pij    = 0.5; % Correlation coefficient (paper has "p"  symbol 
              % for Pearson's correlation is " ρ " when it 
              % is measured in the population
              
%%%%% The paper example is for T = 1, T = 10, T = 100 years
T = 1;
Known_Cmax = 9.96;

% T = 10;
% Known_Cmax = 40.54;

% T = 100;
% Known_Cmax = 74.65;

%%%%% Correlation taken from quantdare.com/correlation-prices-returns/

sigmaij_squared = calc_sigmaij_squared(sigma1, sigma2, pij);
sigmaji_squared = calc_sigmaij_squared(sigma2, sigma1, pij);


N2a1 = d1(S1,X,r,(sigma1*sigma1),T);
N2a2 = d1_prime(S1,S2,sigmaij_squared,T);
N2a3 = p112(sigma1,pij,sigma2,sqrt(sigmaij_squared));
N2a  = [N2a1 N2a2];
E    = [1 N2a3 ; N2a3 1];
p1   = S1*mvncdf(N2a,[],E); % multivariate normal cumulative distribution function;

N2b1 = d1(S2,X,r,(sigma2*sigma2),T);
N2b2 = d1_prime(S2,S1,sigmaij_squared,T);
N2b3 = p112(sigma2,pij,sigma1,sqrt(sigmaji_squared));
N2b  = [N2b1 N2b2];
C    = [1 N2b3 ; N2b3 1];
p2   = S2*mvncdf(N2b,[],C);

N2c1 = d2(S1,X,r,sigma1*sigma1,T);
N2c2 = d2(S2,X,r,sigma2*sigma2,T);
N2c3 = pij;
N2c  = [-N2c1 -N2c2];
D    = [1 N2c3 ; N2c3 1];
p3a1 = 1- mvncdf(N2c,[],D);
p3   = X*(exp(-r*T))*p3a1;

Cmax = p1+p2-p3;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                      %%%
%%%  Print output        %%%
%%%                      %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp(Cmax);
disp(Known_Cmax);
%sprintf(' Cmax = %f Known_Cmax = %f',Cmax,Known_Cmax);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                      %%%
%%%   Helper functions   %%%
%%%                      %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function val = calc_sigmaij_squared(sigma_i, sigma_j, corr_ij )
    val = (sigma_i * sigma_i) - (2 *  corr_ij * sigma_i * sigma_j) + (sigma_j * sigma_j);
end
function val = d1_prime(Si, Sj, sigma_squared, T)
    val = (log(Si/Sj) + (((0.5 * sigma_squared)) * T)) / (sqrt(sigma_squared) * sqrt(T));
end
function val = d2(S, X, r, sigma_squared, T)
    val = (log(S/X) + ((r - (0.5 * sigma_squared)) * T)) / (sqrt(sigma_squared) * sqrt(T));
end
function val = d1(S, X, r, sigma_squared, T)
    val = d2(S,X,r,sigma_squared,T) + (sqrt(sigma_squared) * sqrt(T));
end
function val = p112(sigma1, pij, sigma2, sigma12)
    val = (sigma1 - (pij*sigma2))/sigma12;
end
