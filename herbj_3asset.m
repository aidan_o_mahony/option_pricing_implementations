%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                     %
%   Implementation of pricing of options on the       %
%   minimum or maximum of serveral assets based on    %
%   the paper by Herb Johnson titled                  %
%   "Options on the Maximum or the Minimum of         %
%   Several Assets" from 1987                         %
%                                                     %
%   Implemented by Aidan O Mahony, University         %
%   College Cork, Ireland.                            %
%   Email - 103837793@umail.ucc.ie                    %
%                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%% Values taken from paper %%%%%
n      =   3; % number of assets
S      = [100 100 100];
X      = 100; % Exercise price
r      = 0.1; % Risk free rate 
sigma  = [0.2 0.2 0.2];
rho    = [0.5 0.5 0.5]; %rho12 rho13 rho23

%%%%% The paper example is for T = 1, T = 10, T = 100 years
T = 1;
Known_Cmax = 22.672;

sigma12_squared = calc_sigmaxy_squared(sigma(1), sigma(2), rho(1));
sigma13_squared = calc_sigmaxy_squared(sigma(1), sigma(3), rho(2));
sigma21_squared = calc_sigmaxy_squared(sigma(2), sigma(1), rho(1));
sigma11_squared = calc_sigmaxy_squared(sigma(1), sigma(1), rho(1));
sigma23_squared = calc_sigmaxy_squared(sigma(2), sigma(3), rho(3));

N2a1 = d1(S(1),X,r,sigma11_squared,T);
N2a2 = d1_prime(S(1),S(2),sigma12_squared,T);
N2a3 = d1_prime(S(1),S(3),sigma13_squared,T);
N2a4 = rhoiij(sigma(1),rho(1),sigma(2),sqrt(sigma12_squared)); %rho112
N2a5 = rhoiij(sigma(1),rho(2),sigma(3),sqrt(sigma13_squared)); %rho113
N2a  = [N2a1 N2a2 N2a3];
a    = [1  N2a4 N2a5 ;  
        N2a4  1 N2a4 ; 
        N2a5  N2a4 1];
p1   = S(1)*mvncdf(N2a,[],a); % multivariate normal cumulative distribution function;


N2b1 = d1(S(2),X,r,(sigma(2)*sigma(2)),T);
N2b2 = d1_prime(S(2),S(1),sigma21_squared,T);
N2b3 = d1_prime(S(2),S(3),sigma23_squared,T);
N2b4 = rhoiij(sigma(2),rho(1),sigma(2),sqrt(sigma12_squared)); %rho212
N2b5 = rhoiij(sigma(2),rho(3),sigma(3),sqrt(sigma23_squared)); %rho223
N2b  = [N2b1 N2b2 N2b3];
b    = [1  N2b4 N2b5 ;  
        N2b4  1 N2b4 ; 
        N2b5  N2b4 1];
p2   = S(2)*mvncdf(N2b,[],b); % multivariate normal cumulative distribution function;

N2d1 = d1(S(3),X,r,(sigma(1)*sigma(1)),T);
N2d2 = d1_prime(S(1),S(2),sigma13_squared,T);
N2d3 = d1_prime(S(1),S(3),sigma23_squared,T);
N2d4 = rhoiij(sigma(2),rho(3),sigma(3),sqrt(sigma23_squared)); %rho313
N2d5 = rhoiij(sigma(2),rho(3),sigma(3),sqrt(sigma23_squared)); %rho323
N2d  = [N2d1 N2d2 N2d3];
c    = [1  N2d4 N2d5 ;  
        N2d4  1 N2d4 ; 
        N2d5  N2d4 1  ];
p3   = S(3)*mvncdf(N2d,[],c); % multivariate normal cumulative distribution function;

N2c1 = d2(S(1),X,r,sigma(1)*sigma(1),T);
N2c2 = d2(S(2),X,r,sigma(2)*sigma(2),T);
N2c3 = d2(S(3),X,r,sigma(3)*sigma(3),T);
N2c4 = rho(1);
N2c5 = rho(2);
N2c  = [-N2c1 -N2c2, -N2c3];
h    = [1  N2c4 N2c5 ;  
        N2c4  1 N2c4 ; 
        N2c5  N2c4 1 ];
p3a1 = 1- mvncdf(N2c,[],h);
p4   = X*(exp(-r*T))*p3a1;


Cmax = p1+p2+p3-p4;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                      %%%
%%%  Print output        %%%
%%%                      %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp(Cmax);
disp(Known_Cmax);
%sprintf(' Cmax = %f Known_Cmax = %f',Cmax,Known_Cmax);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                      %%%
%%%   Helper functions   %%%
%%%                      %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function val = calc_sigmaxy_squared(sigma_i, sigma_j, corr_ij )
    val = (sigma_i * sigma_i) - (2 *  corr_ij * sigma_i * sigma_j) + (sigma_j * sigma_j);
end
function val = d1_prime(Si, Sj, sigma_squared, T)
    val = (log(Si/Sj) + (((0.5 * sigma_squared)) * T)) / (sqrt(sigma_squared) * sqrt(T));
end
function val = d2(S, X, r, sigma_squared, T)
    val = (log(S/X) + ((r - (0.5 * sigma_squared)) * T)) / (sqrt(sigma_squared) * sqrt(T));
end
function val = d1(S, X, r, sigma_squared, T)
    val = d2(S,X,r,sigma_squared,T) + (sqrt(sigma_squared) * sqrt(T));
end
function val = rhoiij(sigma1, pij, sigma2, sigma12)
    val = (sigma1 - (pij*sigma2))/sigma12;
end
