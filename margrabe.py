from __future__ import division
from math import log, sqrt, pi
from scipy.stats import norm

s1 = 10
s2 = 20
sigma1 = 1.25
sigma2 = 1.45
t = 0.5
rho = 0.85

def sigma(sigma1, sigma2, rho):
	return sqrt(sigma1**2 + sigma2**2 - 2*rho*sigma1*sigma2)

def d1(s1, s2, t, sigma1, sigma2, rho):
	return (log(s1/s2)+ 1/2 * sigma(sigma1, sigma2, rho)**2 * t)/(sigma(sigma1, sigma2, rho) * sqrt(t))

def d2(s1, s2, t, sigma1, sigma2, rho):
	return d1(s1,s2,t, sigma1, sigma2, rho) - sigma(sigma1, sigma2, rho) * sqrt(t)

def Margrabe(stock1=None, stock2=None, sig1=None, sig2=None, time=None, corr=None):

	if stock1 == None:
		stock1 = s1
	if stock2 == None:
		stock2 = s2
	if time == None:
		time = t
	if sig1 == None:
		sig1 = sigma1
	if sig2 == None:
		sig2 = sigma2
	if corr==None:
		corr = rho

	dd1 = d1(stock1, stock2, time, sig1, sig2, corr)
	dd2 = d2(stock1, stock2, time, sig1, sig2, corr)
	return stock1*norm.cdf(dd1) - stock2*norm.cdf(dd2)

print "Margrabe = " + str(Margrabe())
