# Arithmetic basket call in the standard Black&Scholes model
# https://backtick.se/blog/options-mc-2
import numpy as np
from scipy.stats import norm

n = 2          # number of underlying stocks in the arithmetic basket call
s = 40        # initial stock prices
K = 40         # strike price
sigma = 0.3    # volatility of the stocks
rho = 0.5      # correlations between the stocks
r = 0.1       # risk free interest rate (continuously compounded)
T = 1          # time to maturity (years)

mu = (np.log(s) + (r - 0.5*sigma**2)*T)*np.ones(n)    # expected value vector of the log stock prices

# covariance matrix for the log stock prices
cov_mat = (rho*sigma**2)*np.ones((n,n))
cov_mat = cov_mat - np.diag(np.diag(cov_mat)) + np.diag((sigma**2)*np.ones(n))
cov_mat = cov_mat*T

N = 1000000    # size of the Monte Carlo sample

# generate Monte Carlo sample
mc = np.random.multivariate_normal(mu, cov_mat, N)    # log stock prices
mc = np.exp(mc)                                       # stock prices
mc = np.mean(mc, 1)                                   # arithmeric mean of the stock prices
mc = np.exp(-r*T)*np.maximum(mc - K, 0)               # option prices

price = np.mean(mc)    # estimated price
print("Price = ",price)
# confidence interval
price_std = np.std(mc) / np.sqrt(N)       # standard deviation of the price estimator
lo = price - norm.ppf(0.975)*price_std    # lower bound of confidence interval for the price
hi = price + norm.ppf(0.975)*price_std    # upper bound of confidence interval for the price
