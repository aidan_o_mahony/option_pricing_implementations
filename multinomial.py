"""
Code from Gil
"""

def main():
    """ Code that takes change as input
    and returns the change
    """
    import numpy as np
    tree_depth = int(input("Tree Depth: "))
    num_assets = int(input("Number Of Assets <= 10: "))

    num_assets_p1 = num_assets + 1
    wc = np.array([2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0], dtype=np.int8)
    newp = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], dtype=np.int8)
    done = 0
    while (done == 0):
        print (wc)
        #print (newp)
        endp = wc[num_assets]
        wc[num_assets] = 0
        endp_p1 = endp + 1
        if (endp == tree_depth):
        #if (endp == 3):
            done=1;
        j = num_assets -1
        if (newp[num_assets] == 1):
            wc[num_assets] = 1
            newp[num_assets]=0
        stop = 0
        for i in range(num_assets):
            if (endp == 0 and wc[j] == 1 and stop == 0):
                wc[j] = 0
                wc[j+1] = 1
                stop = 1
            if (endp == 0 and wc[j] == 0 and newp[j] == 1 and stop == 0):
                    wc[j] += 1
                    newp[j] = 0
                    stop = 1
            if (endp != 0 and wc[j] == endp_p1 and j == 0 and stop == 0):
                wc[j+1]   = wc[j]
                wc[j]   += 1
                newp[j+2] = 1
                stop = 1
            if (endp != 0 and wc[j] == endp_p1 and j != 0 and stop == 0):
                wc[j+1]   = endp_p1
                wc[j]     = 0
                newp[j+2] = 1
                stop = 1
            if (endp != 0 and wc[j] >= endp_p1 and j != 0 and stop == 0):
                wc[j+1]   = endp_p1
                newp[j+2] = 1
                stop = 1
            j -= 1 

if __name__== "__main__":
    main()

